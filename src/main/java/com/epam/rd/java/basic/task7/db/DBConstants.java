package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    private DBConstants () {}

    public static final String GET_ALL_USERS = "SELECT * FROM users";
    public static final String INSERT_USERS = "INSERT INTO users VALUES (DEFAULT, ?)";
    public static final String GET_USER_BY_LOGIN = "SELECT * FROM users u WHERE u.login=?";
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM users WHERE login=?";
    public static final String GET_ALL_TEAMS = "SELECT * FROM teams";
    public static final String INSERT_TEAMS = "INSERT INTO teams VALUES (DEFAULT, ?)";
    public static final String GET_TEAM_BY_NAME = "SELECT * FROM teams t WHERE t.name=?";
    public static final String GET_TEAM_BY_ID = "SELECT * FROM teams t WHERE t.id=?";
    public static final String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name=?";
    public static final String UPDATE_TEAM_BY_ID = "UPDATE teams SET name=? WHERE id=?";
    public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    public static final String GET_TEAMS_FOR_USER = "SELECT * FROM users_teams ut WHERE ut.user_id=?";
    public static final String GET_TEAM_FOR_ID = "SELECT * FROM teams t WHERE t.id=?";
}
