package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String FULL_URL = "jdbc:mysql://localhost:3306/testdb?user=root&password=1234";
    private static DBManager instance;
    private static Connection connection;

    public static synchronized DBManager getInstance () {
        if (instance == null) {
            instance = new DBManager ();
        }
        return instance;
    }

    private DBManager () {
//        String URL = null;
//        try (FileInputStream fileInputStream = new FileInputStream ("app.properties")) {
//            Properties properties = new Properties ();
//            properties.load (fileInputStream);
//            URL = properties.getProperty ("connection.url");
//        } catch (IOException e) {
//            e.printStackTrace ();
//        }
//        try {
//            connection = DriverManager.getConnection (URL);
//        } catch (SQLException e) {
//            e.printStackTrace ();
//        }
    }

    public List<User> findAllUsers () throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<User> users = new ArrayList<> ();

        try (Statement stmt = connection.createStatement ();
             ResultSet rs = stmt.executeQuery (DBConstants.GET_ALL_USERS)) {
            while (rs.next ()) {
                User u = new User ();
                u.setId (rs.getInt ("id"));
                u.setLogin (rs.getString ("login"));
                users.add (u);
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Find all users error", e);
        } finally {
            close (connection);
        }

        return users;
    }

    public boolean insertUser (User user) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        PreparedStatement stmt = null;
        try {
            connection.setTransactionIsolation (Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit (false);
            stmt = connection.prepareStatement (DBConstants.INSERT_USERS, Statement.RETURN_GENERATED_KEYS);
            stmt.setString (1, user.getLogin ());
            int count = stmt.executeUpdate ();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys ()) {
                    if (rs.next ()) {
                        user.setId (rs.getInt (1));
                    }
                }
            }
            connection.commit ();
        } catch (SQLException e) {
            e.printStackTrace ();
            rollback (connection);
            throw new DBException ("Insert user error", e);
        } finally {
            close (stmt);
            close (connection);
        }
        return true;
    }

    public boolean deleteUsers (User... users) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement (DBConstants.DELETE_USER_BY_LOGIN);
            for (User u : users) {
                stmt.setString (1, u.getLogin ());
                stmt.executeUpdate ();
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Find all users error", e);
        } finally {
            close (stmt);
            close (connection);
        }

        return true;
    }

    public User getUser (String login) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<User> users = new ArrayList<> ();
        try (PreparedStatement stmt = connection.prepareStatement (DBConstants.GET_USER_BY_LOGIN)) {
            stmt.setString (1, login);
            try (ResultSet rs = stmt.executeQuery ()) {
                if (rs.next ()) {
                    User user = new User ();
                    user.setId (rs.getInt ("id"));
                    user.setLogin (rs.getString ("login"));
                    users.add (user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Get user error", e);
        } finally {
            close (connection);
        }
        if (users.size () == 0) {
            throw new DBException ("Login \"" + login + "\" not found");
        }

        return users.get (0);
    }

    public Team getTeam (String name) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<Team> teams = new ArrayList<> ();
        try (PreparedStatement stmt = connection.prepareStatement (DBConstants.GET_TEAM_BY_NAME)) {
            stmt.setString (1, name);
            try (ResultSet rs = stmt.executeQuery ()) {
                if (rs.next ()) {
                    Team t = new Team ();
                    t.setId (rs.getInt ("id"));
                    t.setName (rs.getString ("name"));
                    teams.add (t);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Get team error", e);
        } finally {
            close (connection);
        }
        if (teams.size () == 0) {
            throw new DBException ("Team \"" + name + "\" not found");
        }

        return teams.get (0);
    }

    public List<Team> findAllTeams () throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<Team> teams = new ArrayList<> ();
        try (Statement stmt = connection.createStatement ();
             ResultSet rs = stmt.executeQuery (DBConstants.GET_ALL_TEAMS)) {
            while (rs.next ()) {
                Team t = new Team ();
                t.setId (rs.getInt ("id"));
                t.setName (rs.getString ("name"));
                teams.add (t);
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Find all teams error", e);
        } finally {
            close (connection);
        }

        return teams;
    }

    public boolean insertTeam (Team team) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        PreparedStatement stmt = null;
        try {
            connection.setTransactionIsolation (Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit (false);
            stmt = connection.prepareStatement (DBConstants.INSERT_TEAMS, Statement.RETURN_GENERATED_KEYS);
            stmt.setString (1, team.getName ());
            int count = stmt.executeUpdate ();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys ()) {
                    if (rs.next ()) {
                        team.setId (rs.getInt (1));
                    }
                }
            }
            connection.commit ();
        } catch (SQLException e) {
            e.printStackTrace ();
            rollback (connection);
            throw new DBException ("Insert team error", e);
        } finally {
            close (stmt);
            close (connection);
        }
        return true;
    }

    public boolean setTeamsForUser (User user, Team... teams) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        PreparedStatement stmt = null;
        try {
            connection.setTransactionIsolation (Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit (false);
            stmt = connection.prepareStatement (DBConstants.SET_TEAMS_FOR_USER, Statement.NO_GENERATED_KEYS);
            for (Team team : teams) {
                stmt.setInt (1, user.getId ());
                stmt.setInt (2, team.getId ());
                stmt.executeUpdate ();
            }
            connection.commit ();
        } catch (SQLException e) {
            e.printStackTrace ();
            rollback (connection);
            throw new DBException ("Set teams for user error", e);
        } finally {
            close (stmt);
            close (connection);
        }
        return true;
    }

    public List<Team> getUserTeams (User user) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<Team> teams = new ArrayList<> ();
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement (DBConstants.GET_TEAMS_FOR_USER);
            stmt.setInt (1, user.getId ());
            try (ResultSet rs = stmt.executeQuery ()) {
                while (rs.next ()) {
                    String name = rs.getString (2);
                    teams.add (getTeamByID (Integer.parseInt (name)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Find all users error", e);
        } finally {
            close (stmt);
            close (connection);
        }

        return teams;
    }

    public Team getTeamByID (int id) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        List<Team> teams = new ArrayList<> ();
        try (PreparedStatement stmt = connection.prepareStatement (DBConstants.GET_TEAM_BY_ID)) {
            stmt.setInt (1, id);
            try (ResultSet rs = stmt.executeQuery ()) {
                if (rs.next ()) {
                    Team t = new Team ();
                    t.setId (rs.getInt ("id"));
                    t.setName (rs.getString ("name"));
                    teams.add (t);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Get team error", e);
        } finally {
            close (connection);
        }
        if (teams.size () == 0) {
            throw new DBException ("Team \"" + id + "\" not found");
        }

        return teams.get (0);
    }

    public boolean deleteTeam (Team team) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        try (PreparedStatement stmt = connection.prepareStatement (DBConstants.DELETE_TEAM_BY_NAME)) {
            stmt.setString (1, team.getName ());
            stmt.executeUpdate ();
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Delete team error", e);
        } finally {
            close (connection);
        }

        return true;
    }

    public boolean updateTeam (Team team) throws DBException {
        try {
            connection = DriverManager.getConnection (CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace ();
        }

        try (PreparedStatement stmt = connection.prepareStatement (DBConstants.UPDATE_TEAM_BY_ID)) {
            stmt.setString (1, team.getName ());
            stmt.setInt (2, team.getId ());
            stmt.executeUpdate ();
        } catch (SQLException e) {
            e.printStackTrace ();
            throw new DBException ("Delete team error", e);
        } finally {
            close (connection);
        }

        return true;
    }

    private void rollback (Connection con) {
        try {
            con.rollback ();
        } catch (SQLException ex) {
            ex.printStackTrace ();
        }
    }

    private void close (AutoCloseable stmt) {
        if (stmt != null) {
            try {
                stmt.close ();
            } catch (Exception e) {
                e.printStackTrace ();
            }
        }
    }
}